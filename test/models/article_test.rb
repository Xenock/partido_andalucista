require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  test "should not save article without titulo" do
    article = Article.new
    assert_not article.save
  end

  test "should save article with titulo and contenido" do
    article = articles(:one)
    assert article.save
  end

end
