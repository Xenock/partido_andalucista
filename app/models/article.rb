class Article < ActiveRecord::Base
  belongs_to :user
  belongs_to :area
  mount_uploader :picture, PictureUploader
  validates :titulo, presence: true, length: { minimum: 5 }
  validates :contenido, presence: true, length: { maximum: 140 }

  def self.filtro_areas(parametro)
    if parametro
      where("area_id =?", parametro)
    else
      all
    end
  end
end