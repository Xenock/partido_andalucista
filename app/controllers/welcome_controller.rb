class WelcomeController < ApplicationController

  def index
    @articles = Article.filtro_areas(params[:area]).page(params[:page]).per(3)
    @areas = Area.all
  end
end
